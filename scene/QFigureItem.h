#ifndef QT_FIGURE_ITEM_H_MRV
#define QT_FIGURE_ITEM_H_MRV

#include <figures.h>

#include <QGraphicsItem>
#include <memory>

class QFigureItem : public QGraphicsItem {
public:
   QFigureItem ( makeFigure::figure_ptr && engine_p, QRectF && rectangle_p );

   void setChecked( bool value_p ) {
      engine_m->setSelected( value_p );
   }

   QRectF boundingRect() const final {
      return rectangle_m;
   }

   void paint ( QPainter * painter_p, const QStyleOptionGraphicsItem * option_p, QWidget * widget_p ) final;
protected:

   void mousePressEvent( QGraphicsSceneMouseEvent * event_p ) final;
private:
   makeFigure::figure_ptr  engine_m;      ///< ������ � �������� ��������� ������
   QRectF                  rectangle_m;   ///< ������������� �������������
}; // class QFigureItem : public QGraphicsItem {

#endif // QT_FIGURE_ITEM_H_MRV
