#ifndef QT_ADD_DIALOG_H_MRV
#define QT_ADD_DIALOG_H_MRV

#include <figures.h>

#include <QLatin1String>
#include <QDialog>
#include <map>
#include <cassert>

class QSpinBox;
class QComboBox;

class QAddDialog : public QDialog {
public:
   QAddDialog ();

   /*!   \brief ��������� ��� ������
   */
   TFigure::figuresTypes_enum figureType () const;
   
   /*!   \brief �������� ������� ��� ���������� ������
   */
   QPointF figurePosition () const;
private:
   using figures_map = std::map< TFigure::figuresTypes_enum, QString >;
   
   QComboBox * figureType_m;   ///< ������� ������ ���� ������
   QSpinBox * x_m, * y_m;    ///< �������� ������� ��������� ������

   static TFigure::figuresTypes_enum int2enum ( int value_p ) {
      const auto result_cl = static_cast< TFigure::figuresTypes_enum >( value_p + 1 );
      assert( TFigure::figuresTypes_enum::EMPTY_FIGURE_TYPE < result_cl );
      return result_cl;
   }

   static int enum2int( TFigure::figuresTypes_enum value_p ) {
      return static_cast< int >( value_p );
   }
   // ���� �����
   static const figures_map & figures ();

   // ��������� ��������
   static void customiseSpin  ( QSpinBox  * element_p );
   static void customiseCombo ( QComboBox * element_p );
};

#endif //QT_ADD_DIALOG_H_MRV