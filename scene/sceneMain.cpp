#include "QDemoWindow.h"

#include <QApplication>
#include <QResource>
#include <memory>

int main ( int argc, char ** argv ) {
   QApplication app_l( argc, argv );
   
   QResource::registerResource( "pics.rcc" );

   const auto mainWnd_cl = std::make_unique< QDemoWindow >();
   mainWnd_cl->show();

   return app_l.exec();
}