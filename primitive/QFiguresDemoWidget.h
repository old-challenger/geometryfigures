#ifndef QT_FIGURES_DEMO_WIDGETS_H_MRV
#define QT_FIGURES_DEMO_WIDGETS_H_MRV

#include "figures.h"
#include "iterators2d.h"

#include <QFrame>
#include <QPoint>
#include <vector>
#include <memory>

class QFiguresDemoWidget : public QFrame {
public:
   using container_type = std::vector< std::unique_ptr< TFigure > >;
   using size_type = int; // container_type::size_type;

   explicit QFiguresDemoWidget ( QString && title_p, 
                                 std::unique_ptr< iterators2D::simple_reverse > && placingIterator_p );

protected:
   void paintEvent( QPaintEvent * event_p ) override;

private:
   std::unique_ptr< iterators2D::simple_reverse >  placingIterator_m;   ///< алгоритм расстановки
   std::vector< std::unique_ptr< TFigure > >       figures_m;           ///< контейнер с объектами-отрисовщиками фигур
   QPoint                                          placingFieldSize_m;  ///< размеры поля для размещения элементов
    
   /*!   \brief метод напонения контейнера figures_m случайными фигурами
   */
   void generateFiguresSet ( size_type figuresAmount_p );
}; // class QFiguresDemoWidget : public QFrame {

#endif // QT_FIGURES_DEMO_WIDGETS_H_MRV
