#ifndef FIGURES_H_MRV
#define FIGURES_H_MRV

#include <memory>
#include <utility>
#include <cassert>
#include <QPen>
#include <QBrush>

class QPainter;
class QPoint;
class QRect;

/*!   \brief базовый класс для отрисовки геометрических фигур
   
*/
class TFigure {
public:
   using enum_type = unsigned short int;

   /*! \brief поддерживаемые типы фигур
   */
   enum class figuresTypes_enum : enum_type {
      EMPTY_FIGURE_TYPE       = 0x0,
      SQUARE_FIGURE_TYPE      = 0x1,
      CIRCLE_FIGURE_TYPE      = 0x2,
      RECTANGLE_FIGURE_TYPE   = 0x3,
      ELLIPSE_FIGURE_TYPE     = 0x4,
      TRIANGLE_FIGURE_TYPE    = 0x5,

      DIFFERENT_FIGURES_AMOUNT = TRIANGLE_FIGURE_TYPE
   };

   static const int  minPrimitiveSize  = 33, ///< линейный размер геометрической фигуры
                     borderSize        = 7;  ///< размер границы

   TFigure ( QPen && pen_p, QBrush && brush_p ) : 
      TFigure( figuresTypes_enum::EMPTY_FIGURE_TYPE, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) { ; }

   /*!   \brief тип фигуры
   */
   figuresTypes_enum type  () const noexcept { return type_m;  }
   const QBrush &    brush () const noexcept { return brush_m; }
   const QPen &      pen   () const noexcept { return pen_m;   }

   bool selected () const noexcept { selected_m; }
   void setSelected ( bool value_p ) noexcept { selected_m = value_p; }

   /*!   \brief отрсовка фигуры в контексе рисования
         \param painter_p - ссылка на контекст рисования QPainter
         \param size_p - текущй размер области 
   */
   virtual void paint ( QPainter & painter_p, const QPoint & size_p ) const;
protected:

   TFigure ( figuresTypes_enum type_p, QPen && pen_p, QBrush && brush_p ) : 
      type_m{ type_p }, pen_m{ pen_p }, brush_m{ brush_p }, selected_m{ false } {; }

   virtual QRect computeRect ( const QPoint & size_p ) const ;
private:
   const figuresTypes_enum type_m;  ///< тип фигуры 
   const QPen              pen_m;   ///< настройка контурной линии
   const QBrush            brush_m; ///< настройка заливки
   
   bool selected_m; ///< пометка выделения объекта
}; // class TFigure {

/*!   \brief класс для отрисовки квадрата

*/
class TSquare : public TFigure {
public:
   TSquare ( QPen && pen_p, QBrush && brush_p ) : 
      TFigure( figuresTypes_enum::SQUARE_FIGURE_TYPE, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) 
   { ; }

   /*!   \brief отрсовка квадрата в контексе рисования
         \param painter_p - ссылка на контекст рисования QPainter
         \param size_p - текущй размер области
   */
   void paint( QPainter & painter_p, const QPoint & size_p ) const override;
protected:

   TSquare( figuresTypes_enum type_p, QPen && pen_p, QBrush && brush_p ) : 
      TFigure( type_p, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) 
   { ; }

   QRect computeRect ( const QPoint & size_p ) const override;
};

/*!   \brief класс для отрисовки круга

*/
class TCircle : public TSquare {
public:
   TCircle ( QPen && pen_p, QBrush && brush_p ) : 
      TSquare( figuresTypes_enum::CIRCLE_FIGURE_TYPE, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) 
   { ; }

   /*!   \brief отрсовка круга в контексе рисования
         \param painter_p - ссылка на контекст рисования QPainter
         \param size_p - текущй размер области
   */
   void paint( QPainter & painter_p, const QPoint & size_p ) const override;
};

/*!   \brief класс для отрисовки прямоугольника

*/
class TRectangle : public TFigure {
public:
   TRectangle ( QPen && pen_p, QBrush && brush_p ) : 
      TFigure( figuresTypes_enum::RECTANGLE_FIGURE_TYPE, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) 
   { ; }

   /*!   \brief отрсовка прямоугольника в контексе рисования
         \param painter_p - ссылка на контекст рисования QPainter
         \param size_p - текущй размер области
   */
   void paint ( QPainter& painter_p, const QPoint & size_p ) const override;
protected:

   TRectangle ( figuresTypes_enum type_p, QPen && pen_p, QBrush && brush_p ) : 
      TFigure( type_p, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) 
   { ; }

   QRect computeRect ( const QPoint & size_p ) const override;
};

/*!   \brief класс для отрисовки треугольника

*/
class TEllipse : public TRectangle {
public:
   TEllipse ( QPen && pen_p, QBrush && brush_p ) : 
      TRectangle( figuresTypes_enum::ELLIPSE_FIGURE_TYPE, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) 
   { ; }

   /*!   \brief отрсовка треугольника в контексе рисования
         \param painter_p - ссылка на контекст рисования QPainter
         \param size_p - текущй размер области
   */
   void paint( QPainter& painter_p, const QPoint& size_p ) const override;
};

/*!   \brief класс для отрисовки треугольника

*/
class TTriangle : public TRectangle {
public:
   TTriangle ( QPen && pen_p, QBrush && brush_p ) : 
      TRectangle( figuresTypes_enum::TRIANGLE_FIGURE_TYPE, std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) ) 
   { ; }

   /*!   \brief отрсовка треугольника в контексе рисования
         \param painter_p - ссылка на контекст рисования QPainter
         \param size_p - текущй размер области
   */
   void paint( QPainter & painter_p, const QPoint & size_p ) const override;
};


class makeFigure {
public:
   using figure_ptr = std::unique_ptr< TFigure >;

   /*!   \brief создание объекта, тип которого задан передаваемым параметром 
         \param type_p - тип создаваемого объекта
         \return unique_ptr на созданный объект
   */
   static figure_ptr runtime ( TFigure::figuresTypes_enum type_p, QPen && pen_p, QBrush && brush_p ) {
      switch ( type_p ) {
      case TFigure::figuresTypes_enum::SQUARE_FIGURE_TYPE:
         return std::make_unique< TSquare >( std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) );
      case TFigure::figuresTypes_enum::CIRCLE_FIGURE_TYPE :
         return std::make_unique< TCircle >( std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) );
      case TFigure::figuresTypes_enum::RECTANGLE_FIGURE_TYPE:
         return std::make_unique< TRectangle >( std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) );
      case TFigure::figuresTypes_enum::ELLIPSE_FIGURE_TYPE :
         return std::make_unique< TEllipse >( std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) );
      case TFigure::figuresTypes_enum::TRIANGLE_FIGURE_TYPE:
         return std::make_unique< TTriangle >( std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) );
      default:
         assert( !"[ERROR] Incorrect switch operator branch" );
      }
      return std::make_unique< TFigure >( std::forward< QPen >( pen_p ), std::forward< QBrush >( brush_p ) );
   }
private:
   makeFigure () = delete;
}; // class makeFigure {

#endif // FIGURES_H_MRV
