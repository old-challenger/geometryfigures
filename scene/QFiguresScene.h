#ifndef QT_FIGURES_SCENE_H_MRV
#define QT_FIGURES_SCENE_H_MRV

#include "QFigureItem.h"
#include <simpleRandom.h>

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QRectF>
#include <QList>

class QFiguresScene : public QGraphicsScene {
public:
   QFiguresScene ( QObject * parent_p );

   void add ( TFigure::figuresTypes_enum type_p, QPointF && position_p );

   void setSelected( QFigureItem * selectedItem_p ) noexcept {
      checkAndUndoSelected();
      selectedItem_m = selectedItem_p;
   }

   void removeSelected () {
      if ( nullptr == selectedItem_m )
         return ;
      removeItem( selectedItem_m );
      selectedItem_m = nullptr;
   }
protected:

   void mousePressEvent ( QGraphicsSceneMouseEvent * mouseEvent_p ) final;
private:
   TFiguresDataGenerator generator_m;
   QFigureItem * selectedItem_m;

   void checkAndUndoSelected ();
}; // class QFiguresScene : public QGraphicsScene {

#endif //QT_FIGURES_SCENE_H_MRV
