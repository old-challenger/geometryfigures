#ifndef QT_DEMO_WINDOW_H_MRV
#define QT_DEMO_WINDOW_H_MRV

#include <QMainWindow.h>

class QAction;
class QSlider;
class QAddDialog;
class QFiguresScene;

/*!   \brief ����� �������� ���� ��� ����������� �������������� ������������ �������
*/
class QDemoWindow : public QMainWindow {
Q_OBJECT
public:

   QDemoWindow ();
   ~QDemoWindow () final;
private slots:

   /*!   \brief ��������� ������� ������������
   */
   void sltReAction();

   /*!   \brief �������������� ������������ �������
   */
   void sltReScale( int );

private:
   QAction * addAct_m, * removeAct_m;  ///< ������� ��������� �������� ������������
   QAddDialog * dialog_m;              ///< ������ ��� ���������� ����� ��������
   QFiguresScene * scene_m;            ///< ������������ �����
   qreal scaleFactor_m;                ///< ������� ������

   /*!   \brief ��������� ��������� ����
   */
   void initMenu ();

   /*!   \brief ��������� �������� ������� ����
   */
   void initCentralWidget ();
}; // class QDemoWindow : public QMainWindow {


#endif // QT_DEMO_WINDOW_H_MRV
