#ifndef ITERATORS_2D_H_MRV
#define ITERATORS_2D_H_MRV

#include <QPoint>
#include <cassert>

/*! \brief итераторы для перебора позиций расставляемых элементов
*/
namespace iterators2D {

class simple_reverse {
public:
   
   explicit simple_reverse ( int elementsAmount_p ) : elementsConter_m( elementsAmount_p ) { ; }

   /*!   \brief условие окончания перебора
         \return true - если перебор закончен, false - если перебор продолжается
   */
   bool finished () const noexcept { return !elementsConter_m; }

   /*!   \brief условие незаконченности перебора
         \return true - если перебор продолжается, false - если перебор закончен
   */
   bool nonempty () const noexcept { return elementsConter_m; }

   /*!   \brief сброс в начальное состояние
   */
   void reset( int elementsAmount_p ) { elementsConter_m = elementsAmount_p; }

   /*!   \brief оставшееся количество перебираемых элементов
   */
   int restElementsCounter() const noexcept { return elementsConter_m; }

   /*!   \brief префиксный переход к следующему значению
   */
   simple_reverse & operator ++ () {
      if ( !elementsConter_m )
         return *this;
      --elementsConter_m; 
      return *this;
   }

   /*!   \brief постфиксный переход к следующему значению
   */
   simple_reverse operator ++ ( int ) {
      if ( !elementsConter_m )
         return *this;
      simple_reverse copy_l( *this );
      --elementsConter_m;
      return std::move( copy_l );
   }

   /*!   \brief оценить размер поля для заданного количества размещаемых элементов
   */
   virtual QPoint computeSizeFor ( int elementsAmount_p ) const noexcept {
      return std::move( QPoint( elementsAmount_p, elementsAmount_p ) );
   }

   /*!   \brief текущее значение
         \return значение типа QPoint
   */
   virtual QPoint value () const noexcept { 
      return std::move( computeSizeFor( elementsConter_m - 1 ) ); 
   }
private:
   int elementsConter_m; ///< счётчик элементов, оставшихся для перебора
}; // class simple_reverse {

class simple_direct : public simple_reverse {
public:

   simple_direct ( int elementsAmount_p ) : simple_reverse( elementsAmount_p ), 
      elementsAmount_cm( elementsAmount_p ) { ; }

   /*!   \brief номер текущего перебираемого элемента
         \return номер элемента, начиная с 0
   */
   int currentElementNumber() const noexcept { return elementsAmount_cm - restElementsCounter(); }

   /*!   \brief изначальное количество перебираемых элементов
    */
   int elementsAmount () const noexcept { return elementsAmount_cm; }

   /*!   \brief текущее значение
         \return значение типа QPoint
   */
   QPoint value () const noexcept override {
      return std::move( computeSizeFor( currentElementNumber() ) );
   }
private:
   const int elementsAmount_cm; ///< изначальное количество перебираемых элементов
}; // class simple_direct : public simple_reverse {

class grid : public simple_direct {
public:
   enum class directions_enum : unsigned char {
      HORISONTAL_DIRECTION = 0x1,   ///< слева на право, сверху вниз
      VERTICAL_DIRECTION   = 0x2    ///< сверху вниз, слева на право
   };

   grid ( int elementsAmount_p, int limitByDirection_p, directions_enum direction_p ) :
      simple_direct( elementsAmount_p ), limitByDirection_cm( limitByDirection_p ), direction_cm( direction_p ) { 
      assert( limitByDirection_cm ); 
   }

   /*!   \brief оценить размер поля для заданного количества размещаемых элементов
   */
   QPoint computeSizeFor( int elementsAmount_p ) const noexcept override {
      const QPoint   complexDivision_cl( complexDivision( elementsAmount_p ) ),
                     result_cl( limitByDirection_cm, complexDivision_cl.y() + ( complexDivision_cl.x() ? 1 : 0 ) );
      return std::move( directions_enum::HORISONTAL_DIRECTION == direction_cm ? result_cl : result_cl.transposed() );
   }
   
   /*!   \brief текущее значение
         \return значение типа QPoint
   */
   QPoint value () const noexcept override {
      const QPoint result_cl = complexDivision( currentElementNumber() );
      return std::move( directions_enum::HORISONTAL_DIRECTION == direction_cm ? result_cl : result_cl.transposed() );
   }
private:
   const int limitByDirection_cm;         ///< максимальное количество элементов по направлению расстановки
   const directions_enum direction_cm;    ///< направление расстановки

   QPoint complexDivision( int value_p ) const noexcept {
      return std::move( QPoint{  value_p % limitByDirection_cm, 
                                 value_p / limitByDirection_cm } );
   }
}; // class grid : public simple_direct {

}; // namespace iterators2D {

#endif // ITERATORS_2D_H_MRV
