#include "QFiguresDemoWidget.h"

#include <QDesktopWidget>
#include <QApplication>
#include <QRect>
#include <vector>
#include <memory>

// расстановка и отображение виджетов верхнего уровня
template< class widgets_container >
void placeWidgets ( const widgets_container & widgets_cp ) {
   if ( widgets_cp.empty() )
      return ;
   QDesktopWidget desktop_l;
   const auto availableGeometry_cl = desktop_l.availableGeometry( widgets_cp.front().get() );
   const QPoint   size_cl( availableGeometry_cl.size().width(), availableGeometry_cl.size().height() ),
                  borderSize_cl = size_cl * .05f,
                  delta_cl = ( size_cl - borderSize_cl * 2 ) / widgets_cp.size() / 3;
   QPoint currentWidgetPosition_l( borderSize_cl );
   for ( const auto & next : widgets_cp ) {
      next->move( currentWidgetPosition_l );
      next->show();
      currentWidgetPosition_l += delta_cl;
   }
}

int main ( int argc, char ** argv ) {
   // количество фиджетов верхнего уровня
   static const auto widgetsAmount_scl = static_cast< std::size_t >( 2 );
   
   QApplication app_l( argc, argv );
   // формирование вектора виджетов верхнего уровня
   auto deleteWidgetLambda_l = []( QFiguresDemoWidget * widget_p ) {
      widget_p->deleteLater();
   };
   using widgets_ptr = std::unique_ptr< QFiguresDemoWidget, decltype( deleteWidgetLambda_l ) >;
   std::vector< widgets_ptr > mainWidgets_l;
   mainWidgets_l.reserve( widgetsAmount_scl );
      mainWidgets_l.emplace_back(       
         new QFiguresDemoWidget{ QString( "simple reverse placing" ), 
                                 std::make_unique< iterators2D::simple_reverse >( 5 ) },
         deleteWidgetLambda_l );
      mainWidgets_l.emplace_back(
         new QFiguresDemoWidget{ QString( "simple direct placing" ), 
                                 std::make_unique< iterators2D::simple_direct >( 7 ) },
         deleteWidgetLambda_l );
      mainWidgets_l.emplace_back(
         new QFiguresDemoWidget{ QString( "horisomtal placing grid" ),
                                 std::make_unique< iterators2D::grid >( 11, 5, 
                                    iterators2D::grid::directions_enum::HORISONTAL_DIRECTION ) },
         deleteWidgetLambda_l );
      mainWidgets_l.emplace_back(
         new QFiguresDemoWidget{ QString( "vertical placing grid" ),
                                 std::make_unique< iterators2D::grid >( 17, 3,
                                    iterators2D::grid::directions_enum::VERTICAL_DIRECTION ) },
         deleteWidgetLambda_l );

   placeWidgets( mainWidgets_l );
   return app_l.exec();
}