#include "simpleRandom.h"

TFiguresDataGenerator::TFiguresDataGenerator() : 
   typesGen_m{ static_cast< TFigure::enum_type >( 1 ), 
               static_cast< TFigure::enum_type >( TFigure::figuresTypes_enum::DIFFERENT_FIGURES_AMOUNT ) }, 
   colorGen_m{ 2, 18 }, int15Gen_m{ 1, 5 }, gradientGen_m{ 0, 3 }, sizeGen_m{ sizesGeneratorLimits_scm / 3, sizesGeneratorLimits_scm }
{ ; }

QBrush TFiguresDataGenerator::generateBrush () {
   static const qreal   gradientSize_scl = 213.,
                        half_scl = gradientSize_scl / 2.;
   enum class gradientType_enum : unsigned char {
      NO_GRADIENT = 0x0,
      LINEAR_GRADIENT = 0x1,
      CONICAL_GRADIENT = 0x2,
      RADIAL_GRADIENT = 0x3
   };
   const auto checked_cl = gradientGen_m.next< gradientType_enum >();
   const auto color_cl = QColor{ colorGen_m.next< Qt::GlobalColor >() };
   switch ( checked_cl ) {
   case gradientType_enum::NO_GRADIENT:
      return std::move( QBrush{ color_cl } );
   case gradientType_enum::LINEAR_GRADIENT: {
      QLinearGradient gradient_l{ 0, 0, gradientSize_scl, gradientSize_scl };
      setUpGradient( gradient_l, color_cl );
      return std::move( QBrush{ gradient_l } );
   }
   case gradientType_enum::CONICAL_GRADIENT: {
      QConicalGradient gradient_l{ half_scl, half_scl, 0 };
      setUpGradient( gradient_l, color_cl );
      return std::move( QBrush{ gradient_l } );
   }
   case gradientType_enum::RADIAL_GRADIENT: {
      static const auto center_scl = QPointF{ half_scl, half_scl };
      QRadialGradient gradient_l{ center_scl, half_scl, center_scl };
      return std::move( QBrush{ gradient_l } );
   }
   default:
      assert(!"[ERROR] Incorrect swicth brush");
      return std::move( QBrush{} );
   };
}

void TFiguresDataGenerator::setUpGradient( QGradient & gradient_p, const QColor & color_cp ) {
   gradient_p.setColorAt(0, Qt::white);
   gradient_p.setColorAt(.5, color_cp);
   gradient_p.setColorAt(1, Qt::white);
}

