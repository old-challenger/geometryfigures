#include "QFiguresDemoWidget.h"
#include <simpleRandom.h>
#include <figures.h>

#include <QPoint>
#include <QPainter>
#include <QPaintEvent>

QFiguresDemoWidget::QFiguresDemoWidget( QString && title_p,  
   std::unique_ptr< iterators2D::simple_reverse > && placingIterator_p ) : 
   QFrame(), placingIterator_m( std::move( placingIterator_p ) ), 
   placingFieldSize_m( placingIterator_m->computeSizeFor( placingIterator_m->restElementsCounter() ) ){
   const auto figuresAmount_cl = placingIterator_m->restElementsCounter();
   generateFiguresSet( figuresAmount_cl );
   const auto minWidgetSize_l = placingFieldSize_m * TFigure::minPrimitiveSize;
   setMinimumSize( minWidgetSize_l.x(), minWidgetSize_l.y() );
   setWindowTitle( title_p );
}

void QFiguresDemoWidget::paintEvent( QPaintEvent * event_p ) {
   QFrame::paintEvent( event_p );
   // определение размера одной фигуры
   const auto figuresAmount_cl = static_cast< int >( figures_m.size() );
   const QPoint figureSize_cl{   event_p->rect().width() / placingFieldSize_m.x(),
                                 event_p->rect().height() / placingFieldSize_m.y() };
   // создание контекста и отрисовка
   QPainter painter_l{ this };
   QPoint placePosition_l; 
   placingIterator_m->reset( figuresAmount_cl );
   for ( auto id = 0; figuresAmount_cl > id; ++id, ++( *placingIterator_m ) ) {
      placePosition_l = placingIterator_m->value();
      painter_l.save();
      painter_l.translate( QPoint{  placePosition_l.x() * figureSize_cl.x(),
                                    placePosition_l.y() * figureSize_cl.y() } );
      figures_m.at( id )->paint( painter_l, figureSize_cl );
      painter_l.restore();
   }
}

void QFiguresDemoWidget::generateFiguresSet( QFiguresDemoWidget::size_type figuresAmount_p ) {
   // подготовка генераторов случайных чисел
   TFiguresDataGenerator generator_l;
   // заполнение контейнера случайными фигурами
   figures_m.reserve( static_cast< container_type::size_type >( figuresAmount_p ) );
   for ( ; figuresAmount_p ; --figuresAmount_p )
      figures_m.push_back( std::move( generator_l.generateFigure() ) );
}

