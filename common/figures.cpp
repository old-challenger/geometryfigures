#include "figures.h"

#include <algorithm>
#include <QPainter>
#include <QPainterPath>

void TFigure::paint( QPainter & painter_p, const QPoint & size_p ) const {
   if ( selected_m ) {
      painter_p.setPen( QPen{ Qt::gray } );
      painter_p.setBrush( QBrush{ Qt::gray } );
      painter_p.drawRect( TFigure::computeRect( size_p ) );
   }
   painter_p.setPen( pen() );
   painter_p.setBrush( brush() );
}

QRect TFigure::computeRect( const QPoint & size_p ) const {
   return std::move( QRect{ 0, 0, size_p.x(), size_p.y() } );
}

void TSquare::paint( QPainter & painter_p, const QPoint & size_p ) const {
   TFigure::paint( painter_p, size_p ); 
   painter_p.drawRect( computeRect( size_p ) );
}

QRect TSquare::computeRect( const QPoint & size_p ) const {
   const auto  maximin_cl        = std::max( std::min( size_p.x(), size_p.y() ), minPrimitiveSize );
   const auto  delta_cl          = QPoint{ maximin_cl, maximin_cl },
               border_cl         = QPoint{ borderSize, borderSize },
               borderedSize_cl   = delta_cl - 2 * border_cl;
   return std::move( QRect{   ( size_p - delta_cl ) / 2 + border_cl,
                              QSize{ borderedSize_cl.x(), borderedSize_cl.y() } });
}

void TCircle::paint( QPainter & painter_p, const QPoint & size_p ) const {
   TFigure::paint( painter_p, size_p ); 
   painter_p.drawEllipse( computeRect( size_p ) );
}

void TRectangle::paint( QPainter& painter_p, const QPoint & size_p ) const {
   TFigure::paint( painter_p, size_p ); 
   painter_p.drawRect( computeRect( size_p ) );
}

QRect TRectangle::computeRect( const QPoint & size_p ) const {
   const auto  border_cl         = QPoint{ borderSize, borderSize },
               borderedSize_cl   = size_p - 2 * border_cl;
   return std::move( QRect{ border_cl, QSize{ borderedSize_cl.x(), borderedSize_cl.y() } } );
}

void TEllipse::paint( QPainter & painter_p, const QPoint & size_p ) const {
   TFigure::paint( painter_p, size_p ); 
   painter_p.drawEllipse( computeRect( size_p ) );
}

void TTriangle::paint( QPainter & painter_p, const QPoint & size_p ) const {
   TFigure::paint(painter_p, size_p); 
   const auto bordersRect_cl = computeRect( size_p );
   const QPoint top_cl{ bordersRect_cl.center().x(), bordersRect_cl.top() };
   QPainterPath path_l{ top_cl };
   path_l.lineTo( bordersRect_cl.bottomLeft() );
   path_l.lineTo( bordersRect_cl.bottomRight() );
   path_l.lineTo( top_cl );
   painter_p.drawPath( path_l );
}