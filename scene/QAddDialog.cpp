#include "QAddDialog.h"

#include <QPushButton>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>

QAddDialog::QAddDialog() : QDialog{ nullptr }, figureType_m{ nullptr }, x_m{ nullptr }, y_m{ nullptr } {
   auto * const topLayout_l = new QVBoxLayout( this );
   
   // �������� ����� ������
   auto * const dataLayout_l = new QGridLayout();
      // �����
      dataLayout_l->addWidget( new QLabel( tr( "figure type: " ),    this ), 0, 0 );
      dataLayout_l->addWidget( new QLabel( tr( "X coordinate: " ),   this ), 1, 0 );
      dataLayout_l->addWidget( new QLabel( tr( "Y coordinate: " ),   this ), 2, 0 );
      dataLayout_l->setColumnStretch( 0, 0 );
      // ���� ������
      dataLayout_l->addWidget( figureType_m = new QComboBox( this ), 0, 1 );
         customiseCombo( figureType_m );
      dataLayout_l->addWidget( x_m = new QSpinBox( this ), 1, 1 );
         customiseSpin( x_m );
      dataLayout_l->addWidget( y_m = new QSpinBox( this ), 2, 1 );
         customiseSpin( y_m );
      dataLayout_l->setColumnStretch( 1, 1 );
   topLayout_l->addLayout( dataLayout_l );

   // �������� ����������
   auto * const buttonsLayout_l = new QHBoxLayout();
      buttonsLayout_l->addStretch( 1 );
      auto * const   okBtn_l     = new QPushButton( QIcon( ":/pics/check24.png" ), tr( "Ok" ),     this ),
           * const   canselBtn_l = new QPushButton( QIcon( ":/pics/cross24.png" ), tr( "Cancel" ), this );
      buttonsLayout_l->addWidget( okBtn_l     );
      buttonsLayout_l->addWidget( canselBtn_l );
      //
      connect( okBtn_l,       SIGNAL( released() ), this, SLOT( accept() ) );
      connect( canselBtn_l,   SIGNAL( released() ), this, SLOT( reject() ) );
   topLayout_l->addLayout( buttonsLayout_l );
   //
   setWindowTitle( tr( "custom new figure" ) );
   setMinimumWidth( 313 );
   setMaximumHeight( 13 );
}

TFigure::figuresTypes_enum QAddDialog::figureType() const {
   return int2enum( figureType_m->currentIndex() );
}

QPointF QAddDialog::figurePosition() const {
   return std::move( QPointF{ static_cast< qreal >( x_m->value() ), 
                              static_cast< qreal >( y_m->value() ) } );
}

const QAddDialog::figures_map & QAddDialog::figures() {
   static const figures_map figuresTypesMap_scl =
      {  {   TFigure::figuresTypes_enum::SQUARE_FIGURE_TYPE,    QLatin1String( "square" )     },
         {   TFigure::figuresTypes_enum::CIRCLE_FIGURE_TYPE,    QLatin1String( "circle" )     },
         {   TFigure::figuresTypes_enum::RECTANGLE_FIGURE_TYPE, QLatin1String( "rectangle" )  },
         {   TFigure::figuresTypes_enum::ELLIPSE_FIGURE_TYPE,   QLatin1String( "ellipse" )    },
         {   TFigure::figuresTypes_enum::TRIANGLE_FIGURE_TYPE,  QLatin1String( "triangle" )   }
      };
   return figuresTypesMap_scl;
}

void QAddDialog::customiseSpin ( QSpinBox * element_p ) {
   element_p->setSingleStep( 10 );
   element_p->setRange( 0, 3000 );
   element_p->setValue( 313 );
}

void QAddDialog::customiseCombo ( QComboBox * element_p ) {
   assert( figures().size() );
   for ( auto it : figures() )
      element_p->addItem( it.second );
   element_p->setCurrentIndex( 0 );
}