#ifndef SIMPLE_RANDOM_H_MRV
#define SIMPLE_RANDOM_H_MRV

#include <figures.h>

#include <QColor>
#include <random>
#include <memory>
#include <cassert>

template< class borders_type >
class TSimpleRandom {
public:
   TSimpleRandom ( borders_type minValue_p, borders_type maxValue_p ) :
      randomDevice_m(), randomGenerator_m( randomDevice_m() ), distribution_m( minValue_p, maxValue_p ) 
   {  assert( minValue_p < maxValue_p ); }

   template< class out_type >
   out_type next () {
      return static_cast< out_type >( distribution_m( randomGenerator_m ) );
   }

private:
   std::random_device   randomDevice_m;
   std::mt19937         randomGenerator_m;
   std::uniform_int_distribution< borders_type > distribution_m; 
}; // class simpleRandom {

class TFiguresDataGenerator {
public:
   TFiguresDataGenerator ();

   /*!   \briaf ��������� ��������� ������� (����� � ���������)
   */
   QBrush generateBrush();

   /*!
   */
   template< class out_type >
   out_type generateRectangle () {
      using simple_type = decltype( out_type().x() );
      return std::move( out_type{ 0, 0, sizeGen_m.next< simple_type >(), sizeGen_m.next< simple_type >() } );
   }

   /*!   \briaf ��������� ���������� ����� �� ������ Qt::GlobalColor
   */
   template< class generator_type >
   QColor generateColor () {
      return std::move( QColor{ colorGen_m.next< Qt::GlobalColor >() } );
   }

   /*!   \brief ��������� ������� ������ �� ���������� �����������
   */
   makeFigure::figure_ptr generateFigure() {
      return std::move( makeFigure::runtime(
         typesGen_m.next< TFigure::figuresTypes_enum >(),
         generatePen(), generateBrush() ) );
   }

   /*!   \briaf ������������ ��������� ����� (�����, �����, ������)
   */
   QPen generatePen () {
      return std::move( QPen{ QBrush{ colorGen_m.next< Qt::GlobalColor >() },
                              int15Gen_m.next< qreal >(), int15Gen_m.next< Qt::PenStyle >() } );
   }

private:
   static const int sizesGeneratorLimits_scm = 333;

   TSimpleRandom< TFigure::enum_type > typesGen_m;             ///< ��������� ��������� �������� 
   TSimpleRandom< int > colorGen_m,    int15Gen_m, 
                        gradientGen_m, sizeGen_m;              ///< ���������� ����� ���������

   /*!   \brief ��������� ���������
   */
   static void setUpGradient ( QGradient & gradient_p, const QColor & color_cp );
}; // class figuresDataGenerator {
#endif // SIMPLE_RANDOM_H_MRV
