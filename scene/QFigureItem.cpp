#include "QFigureItem.h"
#include "QFiguresScene.h"

#include <QPainter>
#include <utility>

QFigureItem::QFigureItem( makeFigure::figure_ptr && engine_p, QRectF && rectangle_p ) :
   QGraphicsItem{ nullptr }, engine_m{ std::forward< makeFigure::figure_ptr >( engine_p ) }, 
   rectangle_m{ std::forward< QRectF >( rectangle_p ) } {
   ; 
}

void QFigureItem::paint( QPainter * painter_p, const QStyleOptionGraphicsItem * option_p, QWidget * widget_p ) {
   painter_p->save();
   engine_m->paint( *painter_p, QPoint( rectangle_m.size().width(), rectangle_m.size().height() ) );
   painter_p->restore();
}

void QFigureItem::mousePressEvent( QGraphicsSceneMouseEvent * event_p ) {
   auto * const scene_l = dynamic_cast< QFiguresScene * >( scene() );
   assert( nullptr != scene_l );
   scene_l->setSelected( this );
   engine_m->setSelected( true );
   update();
   QGraphicsItem::mousePressEvent( event_p );
}

