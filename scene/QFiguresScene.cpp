#include "QFiguresScene.h"
#include "QFigureItem.h"

#include <QGraphicsSceneMouseEvent>

QFiguresScene::QFiguresScene( QObject * parent_p ) : 
   QGraphicsScene{ QRectF( 0., 0., 777., 555. ), parent_p }, 
   generator_m{}, selectedItem_m{ nullptr } {
   ;
}

void QFiguresScene::add( TFigure::figuresTypes_enum type_p, QPointF && position_p ) {   
   auto * const newItem_l = new QFigureItem{ 
      std::move( makeFigure::runtime( type_p, generator_m.generatePen(), generator_m.generateBrush() ) ),
      generator_m.generateRectangle< QRectF >() };
   newItem_l->setPos( position_p );
   newItem_l->setFlags( QGraphicsItem::ItemIsMovable );
   addItem( newItem_l );
}

void QFiguresScene::mousePressEvent( QGraphicsSceneMouseEvent * mouseEvent_p ) {
   QGraphicsScene::mousePressEvent( mouseEvent_p );
   if ( items( mouseEvent_p->scenePos() ).isEmpty() )
      checkAndUndoSelected();
}

void QFiguresScene::checkAndUndoSelected() {
   if ( nullptr != selectedItem_m ) {
      selectedItem_m->setChecked( false );
      selectedItem_m->update();
      selectedItem_m = nullptr;
   }
}

