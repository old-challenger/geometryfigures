#include "QDemoWindow.h"
#include "QFiguresScene.h"
#include "QAddDialog.h"

#include <QGraphicsView>
#include <QToolBar>
#include <QMenuBar>
#include <QDialog>
#include <QSlider>
#include <QMenu>
#include <cassert>

QDemoWindow::QDemoWindow() : QMainWindow{ nullptr }, 
   addAct_m{ nullptr }, removeAct_m{ nullptr }, 
   dialog_m{ nullptr }, scene_m{ nullptr }, scaleFactor_m{ .7 } {
   initMenu();
   initCentralWidget();
   dialog_m = new QAddDialog();
}

QDemoWindow::~QDemoWindow() {
   dialog_m->deleteLater();
}

void QDemoWindow::sltReAction () {
   auto * const sender_l = dynamic_cast< QAction * >( sender() );
   assert( nullptr != sender_l || !"[ERROR] incorrect signal sender type" );
   if ( sender_l == addAct_m ) {
      if ( QDialog::Accepted == static_cast< QDialog::DialogCode >( dialog_m->exec() ) ) {
         scene_m->add( dialog_m->figureType(), dialog_m->figurePosition() );
      }
   } else if ( sender_l == removeAct_m ) {
      scene_m->removeSelected();
   } else 
      assert( !"[ERROR] incorrect signal sender action" );
}

void QDemoWindow::sltReScale( int scalePercents_p ) {
   const auto scale_cl = static_cast< qreal >( scalePercents_p ) / 100.;
   auto * const view_l = dynamic_cast< QGraphicsView * >( centralWidget() );
   view_l->scale( scale_cl / scaleFactor_m, scale_cl / scaleFactor_m );
   scaleFactor_m = scale_cl;
   view_l->update();
}

void QDemoWindow::initMenu() {
   // ����
   auto * const menu_l = menuBar()->addMenu( tr( "&actions" ) );
   addAct_m = menu_l->addAction( QIcon{ ":/pics/plus24.png" }, tr( "&add" ) );
   removeAct_m = menu_l->addAction( QIcon{ ":/pics/cross24.png" }, tr( "&remove" ) );
   menu_l->addSeparator();
   auto * const exitAct_l = menu_l->addAction( tr( "e&xit" ) );
   // ������ ������������
   auto * const toolBar_l = addToolBar( "main" );
   toolBar_l->addAction( addAct_m );
   toolBar_l->addAction( removeAct_m );
   toolBar_l->addSeparator();
   auto * const slider_l = new QSlider{ Qt::Horizontal, this };
      slider_l->setRange(10, 100);
      slider_l->setValue( static_cast< int >( slider_l->maximum() * scaleFactor_m ) );
   toolBar_l->addWidget( slider_l );
   toolBar_l->addWidget( new QWidget() );

   connect( slider_l, SIGNAL( valueChanged( int ) ), this, SLOT( sltReScale( int ) ) );
   connect( addAct_m,      SIGNAL( triggered() ), this, SLOT( sltReAction() ) );
   connect( removeAct_m,   SIGNAL( triggered() ), this, SLOT( sltReAction() ) );
   connect( exitAct_l,     SIGNAL( triggered() ), this, SLOT( close()       ) );
}

void QDemoWindow::initCentralWidget() {
   scene_m = new QFiguresScene{ this };
   auto * const centralWidget_l = new QGraphicsView{ scene_m, this };
      centralWidget_l->setHorizontalScrollBarPolicy( Qt::ScrollBarPolicy::ScrollBarAsNeeded );
      centralWidget_l->setVerticalScrollBarPolicy( Qt::ScrollBarPolicy::ScrollBarAsNeeded );
   setCentralWidget( centralWidget_l );
}
